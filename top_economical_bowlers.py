from matplotlib import pyplot as plt
import helper


def top_economical_bowlers():
    id_list = helper.open_matches_file('matches.csv', 2015)
    deliveries_list = helper.open_deliveries_file('deliveries.csv', id_list)

    total_runs_dict = {}
    total_balls_dict = {}

    total_balls_list = []
    total_runs_list = []
    bowlers_list = []
    bowler_economy_dict = {}
    top_10_economy_list = []

    for elem in deliveries_list:
        if elem['is_super_over'] == '0':
            total_runs_per_ball = int(
                elem['total_runs']) - int(elem['bye_runs']) - int(elem['legbye_runs'])

            if elem['bowler'] in total_runs_dict:
                total_runs_dict[elem['bowler']] += total_runs_per_ball
            else:
                total_runs_dict[elem['bowler']] = total_runs_per_ball

    for elem in deliveries_list:
        if elem['is_super_over'] == '0':
            if elem['bowler'] in total_balls_dict:
                # if int(elem['noball_runs'])>0 or int(elem['wide_runs'])>0:
                #     pass
                # else:
                total_balls_dict[elem['bowler']] += 1
            else:
                # if int(elem['noball_runs'])>0 or int(elem['wide_runs'])>0:
                #     pass
                # else:
                total_balls_dict[elem['bowler']] = 1
    total_balls_list = list(total_balls_dict.values())
    total_runs_list = list(total_runs_dict.values())
    bowlers_list = list(total_balls_dict.keys())

    for x in range(0, len(total_balls_list)):
        bowler_economy_dict[bowlers_list[x]] = (
            total_runs_list[x]*6)/total_balls_list[x]

    top_10_economy_list = sorted(list(bowler_economy_dict.values()))[0:10]
    print(top_10_economy_list)
    top_10_bowler_name_list = []
    for x in top_10_economy_list:
        for name, value in bowler_economy_dict.items():
            if x == value:
                top_10_bowler_name_list.append(name)
    print(top_10_bowler_name_list)
    plt.plot(top_10_bowler_name_list, top_10_economy_list)
    plt.xticks(top_10_bowler_name_list, rotation='vertical')
    plt.show()


top_economical_bowlers()
