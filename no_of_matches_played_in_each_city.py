from matplotlib import pyplot as plt
import csv


def no_of_matches_played_in_each_city():
    with open('matches.csv', 'r') as matches_file:
        reader = csv.DictReader(matches_file)
        # print(reader)
        matches_list = []
        for row in reader:
            matches_list.append(dict(row))
    # print(matches_list)
    matches_file.close()
    mydict = {}
    for elem in matches_list:
        if elem['city'] in mydict:
            mydict[elem['city']] += 1
        else:
            mydict[elem['city']] = 1
    print(mydict)
    x = list(mydict.keys())
    y = list(mydict.values())

    plt.plot(x, y) 
    plt.xticks(x, rotation='vertical')
    plt.show()


no_of_matches_played_in_each_city()