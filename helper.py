import csv
import matplotlib
matplotlib.use('TkAgg')


# def open_matches_file():
#     with open('matches.csv', 'r') as matches_file:
#         reader = csv.DictReader(matches_file)
#         # print(reader)
#         matches_list = []
#         for row in reader:
#             matches_list.append(dict(row))
#     # print(matches_list)
#     matches_file.close()
#     return matches_list


def open_matches_file(filename, season):
    with open(filename, 'r') as matches_file:
        reader = csv.DictReader(matches_file)

        # print(reader)
        matches_list = []
        for row in reader:
            if int(row['season']) == season:
                matches_list.append(row['id'])
    # print(matches_list)
    matches_file.close()
    return matches_list


open_matches_file('matches.csv', 2016)


def open_deliveries_file(filename, matches_list):
    with open(filename, 'r') as deliveries_file:
        reader = csv.DictReader(deliveries_file)
        deliveries_list = []
        for row in reader:
            for elem in matches_list:
                if row['match_id'] == elem:
                    deliveries_list.append(dict(row))
            # deliveries_list.append(dict(row))
    deliveries_file.close()
    return deliveries_list

