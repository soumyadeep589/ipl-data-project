from matplotlib import pyplot as plt
import csv


def matches_own_by_all_team_of_all_year():
    with open('matches.csv', 'r') as matches_file:
        reader = csv.DictReader(matches_file)
        # print(reader)
        matches_list = []
        for row in reader:
            matches_list.append(dict(row))
    # print(matches_list)
    matches_file.close()
    mydict = {}
    for elem in matches_list:
        if elem['winner'] in mydict:
            if elem['season'] in mydict[elem['winner']]:
                mydict[elem['winner']][elem['season']] += 1
            else:
                mydict[elem['winner']][elem['season']] = 1 
        else:
            mydict[elem['winner']] = {}
    # print(mydict)
    winner_list = []
    for elem in mydict.keys():
        winner_list.append(elem)
      
    print(winner_list)
    season_list = []
    win_num_list = []
    for x in range(2008, 2018):
        season_list.append(x)
    print(season_list)
    
    for elem in mydict.values():
        win_list = []
        for x in season_list:
            if str(x) in elem.keys():
                win_list.append(elem[str(x)])
            else:
                win_list.append(0) 
        win_num_list.append(win_list)
    print(win_num_list)
    
    
    def add_bottom(i):
        bottom = []
        for x in season_list:
            bottom.append(0)
        if i == 0:
            return bottom
        else:
            for j in range(i):
                for k in range(len(season_list)):
                    bottom[k] += win_num_list[j][k]
            return bottom
    
    for i in range(len(winner_list)):
        plt.bar(season_list, win_num_list[i], bottom = add_bottom(i))

    plt.show()


matches_own_by_all_team_of_all_year()