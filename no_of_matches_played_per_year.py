from matplotlib import pyplot as plt
import csv


def no_of_matches_played_per_year():
    with open('matches.csv', 'r') as matches_file:
        reader = csv.DictReader(matches_file)
        # print(reader)
        matches_list = []
        for row in reader:
            matches_list.append(dict(row))
    # print(matches_list)
    matches_file.close()
    mydict = {}
    for elem in matches_list:
        if elem['season'] in mydict:
            mydict[elem['season']] += 1
        else:
            mydict[elem['season']] = 1
    
    print(mydict)
    x = sorted(list(mydict.keys()))
    y = list(mydict.values())
    value_list = []
    for i in x:
        for year, value in mydict.items():
            if i == year:
                value_list.append(value)
    
    plt.plot(x, value_list) 
    plt.show() 
    # return mydict


no_of_matches_played_per_year()