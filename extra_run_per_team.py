from matplotlib import pyplot as plt
import helper


def extra_run_per_team():
    id_list = helper.open_matches_file('matches.csv', 2016)
    deliveries_list = helper.open_deliveries_file('deliveries.csv', id_list)
    mydict = {}
    
    # for elem in deliveries_list:
    #     if int(elem['is_super_over'], 10) > 0:
    #         mydict[elem['match_id']] = elem['is_super_over']
    
    for elem in deliveries_list:
        if elem['is_super_over'] == '0':
            if elem['bowling_team'] in mydict:
                mydict[elem['bowling_team']] += int(elem['extra_runs'])
            else:
                mydict[elem['bowling_team']] = int(elem['extra_runs'])
    print(mydict)
    x = list(mydict.keys())
    y = list(mydict.values())
    
    plt.plot(x, y) 
    plt.xticks(x, rotation='vertical')
    plt.show() 


extra_run_per_team()